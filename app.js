const logger = require("./logger");
require("dotenv").config();

const api = require("./api");

// Start the server
api.listen(process.env.PORT || 8000, () => {
  logger.info("server started");
});
