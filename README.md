# reunion-backend-assignment

## Dummy data
Dummy data used is available [./dummy_data.sql](./dummy_data.sql)

All users have the following password: `demo`

Demo User:
```
email: akohnert0@cnbc.com
password: demo
```

## API Docs
Available at [/api/api-docs](https://reunion-backend-stkb.onrender.com/api/api-docs)

API docs are available in OpenAPI format which is also used to validate the request body using [express-openapi-validator](https://www.npmjs.com/package/express-openapi-validator).

## Setup
### Environment Variables
```
NODE_ENV: <production | ...>
DB_URI: Postgres database URI
JWT_PUBLIC_KEY: JWT RS512 Public Key
JWT_PRIVATE_KEY: JWT RS512 Private Key
JWT_EXPIRES_IN: JWT expiry period
PORT: port to listen on
SERVICE_NAME: application name
```

## Deployment
### Docker

**Build**: `docker build -t social-media .`

**Run**: `docker run --env-file .env -d social-media`

### Render
[![Deploy to Render](https://render.com/images/deploy-to-render-button.svg)](https://render.com/deploy?repo=https://gitlab.com/crossphoton/reunion-backend-assignment)

