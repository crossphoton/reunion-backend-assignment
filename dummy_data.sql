-- users
-- All of the password are 'demo'
insert into users (id, email, password, "createdAt", "updatedAt") values ('b49a72a0-c722-4adb-897c-3925ec2f0e58', 'akohnert0@cnbc.com', '$2a$10$ZJK45IYJuRqblOEVP/xe4ukQV0qXtj1IlwTNSIUacTfy3YLMAAsZW', '11/27/2022', '1/3/2023');
insert into users (id, email, password, "createdAt", "updatedAt") values ('54b21669-3e1b-4297-8841-bc42166fa732', 'mhalms1@mediafire.com', '$2a$10$ZJK45IYJuRqblOEVP/xe4ukQV0qXtj1IlwTNSIUacTfy3YLMAAsZW', '1/12/2022', '2/8/2022');
insert into users (id, email, password, "createdAt", "updatedAt") values ('b6c569e1-0a44-41c8-8e2b-4ac29ea28776', 'prudge2@europa.eu', '$2a$10$ZJK45IYJuRqblOEVP/xe4ukQV0qXtj1IlwTNSIUacTfy3YLMAAsZW', '2/19/2022', '12/22/2022');
insert into users (id, email, password, "createdAt", "updatedAt") values ('ed2aab2f-8650-4fc9-99e6-402153e36482', 'fhaliday3@ucoz.ru', '$2a$10$ZJK45IYJuRqblOEVP/xe4ukQV0qXtj1IlwTNSIUacTfy3YLMAAsZW', '2/6/2022', '5/16/2022');
insert into users (id, email, password, "createdAt", "updatedAt") values ('26634fde-bcfe-4144-bf1a-1fc6740cec66', 'gbartaletti4@dot.gov', '$2a$10$ZJK45IYJuRqblOEVP/xe4ukQV0qXtj1IlwTNSIUacTfy3YLMAAsZW', '7/24/2022', '6/9/2022');

-- followers
insert into followers (follower_id, following_id, "createdAt", "updatedAt") values ('b49a72a0-c722-4adb-897c-3925ec2f0e58', '54b21669-3e1b-4297-8841-bc42166fa732', now(), now());
insert into followers (follower_id, following_id, "createdAt", "updatedAt") values ('54b21669-3e1b-4297-8841-bc42166fa732', 'b49a72a0-c722-4adb-897c-3925ec2f0e58', now(), now());
insert into followers (follower_id, following_id, "createdAt", "updatedAt") values ('b6c569e1-0a44-41c8-8e2b-4ac29ea28776', 'b49a72a0-c722-4adb-897c-3925ec2f0e58', now(), now());

-- posts
insert into posts (id, user_id, title, description, "createdAt", "updatedAt") values ('07dc9902-8f3e-11ed-a1eb-0242ac120002', 'b49a72a0-c722-4adb-897c-3925ec2f0e58', 'First post', 'Hi, this is my first post', now(), now());
insert into posts (id, user_id, title, description, "createdAt", "updatedAt") values ('07dc9bb4-8f3e-11ed-a1eb-0242ac120002', 'b49a72a0-c722-4adb-897c-3925ec2f0e58', 'Second post', 'Hi, this is my Second post', now(), now());
insert into posts (id, user_id, title, description, "createdAt", "updatedAt") values ('07dc9cea-8f3e-11ed-a1eb-0242ac120002', 'b49a72a0-c722-4adb-897c-3925ec2f0e58', 'Third post', 'Hi, this is my third post', now(), now());

-- likes
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9cea-8f3e-11ed-a1eb-0242ac120002', '26634fde-bcfe-4144-bf1a-1fc6740cec66', now(), now());
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9cea-8f3e-11ed-a1eb-0242ac120002', 'ed2aab2f-8650-4fc9-99e6-402153e36482', now(), now());
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9cea-8f3e-11ed-a1eb-0242ac120002', 'b6c569e1-0a44-41c8-8e2b-4ac29ea28776', now(), now());
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9cea-8f3e-11ed-a1eb-0242ac120002', '54b21669-3e1b-4297-8841-bc42166fa732', now(), now());
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9cea-8f3e-11ed-a1eb-0242ac120002', 'b49a72a0-c722-4adb-897c-3925ec2f0e58', now(), now());
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9bb4-8f3e-11ed-a1eb-0242ac120002', 'ed2aab2f-8650-4fc9-99e6-402153e36482', now(), now());
insert into likes (post_id, user_id, "createdAt", "updatedAt") values ('07dc9bb4-8f3e-11ed-a1eb-0242ac120002', '26634fde-bcfe-4144-bf1a-1fc6740cec66', now(), now());

-- comments
insert into comments (id, post_id, user_id, comment, "createdAt", "updatedAt") values ('b6f243a0-8f76-11ed-a1eb-0242ac120002', '07dc9cea-8f3e-11ed-a1eb-0242ac120002', '26634fde-bcfe-4144-bf1a-1fc6740cec66', 'This is a comment', now(), now());
insert into comments (id, post_id, user_id, comment, "createdAt", "updatedAt") values ('b6f24602-8f76-11ed-a1eb-0242ac120002', '07dc9cea-8f3e-11ed-a1eb-0242ac120002', '26634fde-bcfe-4144-bf1a-1fc6740cec66', 'Hola, Amigo', now(), now());
insert into comments (id, post_id, user_id, comment, "createdAt", "updatedAt") values ('b6f24738-8f76-11ed-a1eb-0242ac120002', '07dc9cea-8f3e-11ed-a1eb-0242ac120002', '26634fde-bcfe-4144-bf1a-1fc6740cec66', 'Un Poco Loco', now(), now());
insert into comments (id, post_id, user_id, comment, "createdAt", "updatedAt") values ('b6f2485a-8f76-11ed-a1eb-0242ac120002', '07dc9bb4-8f3e-11ed-a1eb-0242ac120002', '26634fde-bcfe-4144-bf1a-1fc6740cec66', 'This is going to be great', now(), now());
