const { request, response } = require("express");
const logger = require("../../logger");
const Post = require("../models/post");
const { db } = require("../database/db");

module.exports = {
  async add_post(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const { title, description } = req.body;
      const result = await Post.create({
        title,
        description,
        user_id: req.locals.user.id,
      });
      res.json(result.dataValues);
    } catch (error) {
      logger.error("error while following", error);
      res.status(500).send(error.message);
    }

    next();
  },
  async delete_post(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const result = await Post.destroy({
        where: { id, user_id: req.locals.user.id },
      });
      res.sendStatus(result > 0 ? 204 : 405);
    } catch (error) {
      logger.error("error while following", error);
      res.status(500).send(error.message);
    }

    next();
  },
  async get_post(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const { id } = req.params;
      const [result, _] = await db.query(`
select
  *,
  COALESCE((select count(*) from likes where post_id='${id}'), 0) as likes,
  ((
    select array_agg(array[comment, users.email::text])
    from comments inner join users on comments.user_id=users.id
    where post_id='${id}' group by post_id)
    ) as comments
from posts where id='${id}'`);
      res.json(result && result[0]);
    } catch (error) {
      logger.error("error in get_post", error);
      res.status(500).send(error.message);
    }

    next();
  },
  async get_all_post(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const [result, _] = await db.query(`
select
  *,
  COALESCE((select count(*) from likes where post_id=posts.id), 0) as likes,
  ((
    select array_agg(array[comment, users.email::text])
    from comments inner join users on comments.user_id=users.id
    where post_id=posts.id group by post_id)
    ) as comments
from posts where user_id='${req.locals.user.id}'
group by id;`);
      res.json(result);
    } catch (error) {
      logger.error("error get_all_post", error);
      res.status(500).send(error.message);
    }

    next();
  },
};
