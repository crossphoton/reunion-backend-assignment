const { request, response } = require("express");
const logger = require("../../logger");
const Follower = require("../models/follower");

module.exports = {
  async add_follower(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const following_id = req.params.id;
      await Follower.create({ follower_id: req.locals.user.id, following_id });
      res.sendStatus(204);
    } catch (error) {
      logger.error("error while following", error);
      res.status(500).send(error.message);
    }

    next();
  },
  async delete_follower(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const following_id = req.params.id;
      await Follower.destroy({ follower_id: req.locals.user.id, following_id });
      res.sendStatus(204);
    } catch (error) {
      logger.error("error while unfollowing", error);
      res.status(500).send(error.message);
    }

    next();
  },
};
