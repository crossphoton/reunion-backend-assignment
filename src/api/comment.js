const { request, response } = require("express");
const Comment = require("../models/comment");
const logger = require("../../logger");

module.exports = {
  async add_comment(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const { id: post_id } = req.params;
      const { comment } = req.body;

      const result = await Comment.create({
        post_id,
        comment,
        user_id: req.locals.user.id,
      });
      res.json({ id: result.dataValues.id });
    } catch (error) {
      logger.error("error in add comment", error);
      res.status(500).send(error.message);
    }

    next();
  },
};
