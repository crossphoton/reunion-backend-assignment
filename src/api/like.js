const { request, response } = require("express");
const logger = require("../../logger");
const Like = require("../models/like");

module.exports = {
  async add_like(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const { id: post_id } = req.params;
      await Like.create({ post_id, user_id: req.locals.user.id });
    } catch (error) {}
    res.sendStatus(204);
    next();
  },
  async delete_like(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      const { id: post_id } = req.params;
      await Like.destroy({ where: { post_id, user_id: req.locals.user.id } });
    } catch (error) {}
    res.sendStatus(204);
    next();
  },
};
