const { request, response } = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const Follower = require("../models/follower");
const { db } = require("../database/db");
const logger = require("../../logger");

module.exports = {
  async authenticate(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      if (req.locals && req.locals.user) return res.sendStatus(405);
      const { email, password } = req.body;
      if (!email || !password) throw Error("Invalid body");

      const user = await User.findOne({ where: { email } });
      if (!user) return res.sendStatus(401);

      // Check password
      const compare_res = bcrypt.compareSync(password, user.password);
      if (!compare_res) return res.sendStatus(401);

      // Generate token
      const user_data = { id: user.id, email: user.email };
      const token = jwt.sign(user_data, process.env.JWT_PRIVATE_KEY, {
        expiresIn: process.env.JWT_EXPIRES_IN || "3h",
        algorithm: "RS512",
      });

      res.json({ token });
    } catch (error) {
      logger.error("error: api/user.js/authenticate", error);
      res.status(500).send(error.message);
    }

    next();
  },
  async get_current_user(
    /** @type {request} */ req,
    /** @type {response} */ res,
    next
  ) {
    try {
      // Count follower counts in single query
      var [result, _] = await db.query(`select
    (select count(*) from followers where follower_id='${req.locals.user.id}') as following, 
    (select count(*) from followers where following_id='${req.locals.user.id}') as followers;`);

      result = result[0];
      res.json({ ...result, username: req.locals.user.email });
    } catch (error) {
      logger.error("error: api/user.js/authenticate", error);
      res.status(500).send(error.message);
    }
    next();
  },
};
