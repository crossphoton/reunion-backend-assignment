const { Router } = require("express");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../openapi.json");

const middlewares = { ...require("./middlewares/auth") };

const { authenticate, get_current_user } = require("./api/user");
const { add_follower, delete_follower } = require("./api/follower");
const { add_post, delete_post, get_post, get_all_post } = require("./api/post");
const { add_like, delete_like } = require("./api/like");
const { add_comment } = require("./api/comment");

const app = Router();

// Swagger API Spec
app.use("/api-docs", swaggerUi.serve);
app.get("/api-docs", swaggerUi.setup(swaggerDocument));

// Decode authentication token
app.use(middlewares.check_auth);

// Health check
app.get("/healthz", (_req, res) => res.sendStatus(200));

///////// Users endpoints
app.post("/authenticate", authenticate);
app.get("/user", middlewares.authenticated_only, get_current_user);

//////// Follower endpoints
app.post("/follow/:id", middlewares.authenticated_only, add_follower);
app.post("/unfollow/:id", middlewares.authenticated_only, delete_follower);

//////// Post endpoints
app.post("/posts", middlewares.authenticated_only, add_post);
app.delete("/posts/:id", middlewares.authenticated_only, delete_post);
app.get("/posts/:id", middlewares.authenticated_only, get_post);
app.get("/all_posts", middlewares.authenticated_only, get_all_post);

//////// Like endpoints
app.post("/like/:id", middlewares.authenticated_only, add_like);
app.post("/unlike/:id", middlewares.authenticated_only, delete_like);

//////// Comment endpoints
app.post("/comment/:id", middlewares.authenticated_only, add_comment);

module.exports = app;
