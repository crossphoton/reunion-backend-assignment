const { DataTypes, Sequelize } = require("sequelize");
const { db } = require("../database/db");

const Comment = db.define(
  "comments",
  {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
    },
    post_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "posts", key: "id" },
    },
    user_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "users", key: "id" },
    },
    comment: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  },
  {
    timestamps: true,
    tableName: "comments",
    indexes: [{ unique: false, fields: ["post_id"] }],
  }
);

module.exports = Comment;
