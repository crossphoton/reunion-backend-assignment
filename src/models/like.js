const { DataTypes } = require("sequelize");
const { db } = require("../database/db");

const Like = db.define(
  "likes",
  {
    post_id: {
      primaryKey: true,
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "posts", key: "id" },
    },
    user_id: {
      primaryKey: true,
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "users", key: "id" },
    },
  },
  {
    timestamps: true,
    tableName: "likes",
    indexes: [{ unique: false, fields: ["post_id"] }],
  }
);

module.exports = Like;
