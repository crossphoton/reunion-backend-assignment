const { DataTypes, Sequelize } = require("sequelize");
const { db } = require("../database/db");

const User = db.define(
  "users",
  {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    timestamps: true,
    tableName: "users",
    indexes: [{ unique: false, fields: ["email"] }],
  }
);

module.exports = User;
