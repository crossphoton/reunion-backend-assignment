const { DataTypes } = require("sequelize");
const { db } = require("../database/db");

const Follower = db.define(
  "followers",
  {
    follower_id: {
      primaryKey: true,
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "users", key: "id" },
    },
    following_id: {
      primaryKey: true,
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "users", key: "id" },
    },
  },
  {
    timestamps: true,
    tableName: "followers",
    indexes: [
      { unique: false, fields: ["follower_id"] },
      { unique: false, fields: ["following_id"] },
    ],
  }
);

module.exports = Follower;
