const { DataTypes, Sequelize } = require("sequelize");
const { db } = require("../database/db");

const Post = db.define(
  "posts",
  {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
    },
    user_id: {
      type: DataTypes.UUID,
      allowNull: false,
      references: { model: "users", key: "id" },
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  },
  {
    timestamps: true,
    tableName: "posts",
    indexes: [{ unique: false, fields: ["user_id"] }],
  }
);

module.exports = Post;
