const { Sequelize } = require("sequelize");

// Setup a DB connection using URI with connection pool
const db = new Sequelize(process.env.DB_URI, {
  pool: { max: 5, min: 0 },
  logging: false,
});

const close = () => {
  return db.close();
};

module.exports = { db, close };
