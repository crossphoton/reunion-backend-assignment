const { response, request } = require("express");
const jwt = require("jsonwebtoken");
const logger = require("../../logger");

// Middleware to check Authorization header
const check_auth = (
  /** @type {request} */ req,
  /** @type {response} */ res,
  next
) => {
  if (!req.get("authorization")) {
    return next();
  }
  var token = req.get("authorization");
  if (token.split(" ").length < 2) return next();
  token = token.split(" ")[1];
  try {
    const result = jwt.verify(token, process.env.JWT_PUBLIC_KEY, {
      algorithms: "RS512",
    });
    logger.debug("jwt data", result);
    req.locals = { ...req.locals, user: result };
    return next();
  } catch (error) {
    logger.debug("error in jwt verify", error);
    return next();
  }
};

// Should be used after check_auth middleware
// Return 401 for unauthorized requests
const authenticated_only = (
  /** @type {request} */ req,
  /** @type {response} */ res,
  next
) => {
  if (!req.locals || req.locals.user == {}) return res.sendStatus(401);
  return next();
};

module.exports = { check_auth, authenticated_only };
