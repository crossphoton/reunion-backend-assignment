const express = require("express");
const path = require("path");
const app = express();
const api = require("./src/api");
const morgan = require("morgan");
const OpenApiValidator = require("express-openapi-validator");

const apiSpec = path.join(__dirname, "openapi.json");

app.use(express.json({}));
app.use(express.urlencoded({ extended: true }));
app.use(morgan("tiny"));
app.use(
  OpenApiValidator.middleware({
    apiSpec,
    validateResponses: false,
    validateRequests: true,
    ignoreUndocumented: true,
  })
);

app.use("/api", api);

module.exports = app;
